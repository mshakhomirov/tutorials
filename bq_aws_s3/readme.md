# bq-aws-s3 AWS lambda function

Lambda Function which runs BigQuery jobs programmatically and saves the result to AWS S3.

## Installation

1. install AWS CLI first
2. Run `deploy.sh` to deploy the function.
3. Alternatively run this to create a function from command line:


## Running locally

I am using `./test.js` file in our project.

`./event.json` file simulates AWS S3 bucket Create object event. 

To trigger the local lambda in your command line run:

~~~~bash
node test
~~~~
                                   