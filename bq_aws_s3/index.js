'use strict';
const AWS = require('aws-sdk');
const { Storage } = require('@google-cloud/storage');
const { BigQuery } = require('@google-cloud/bigquery');
AWS.config.update({region: "eu-west-1"});
let s3 = new AWS.S3();
const moment = require('moment');

let config = require('./config.json');
const environment = process.env.ENVIRONMENT;

exports.handler = async (event, context) => {
    let tables = config.Tables;
    for (const table of tables) {
            console.log('Loading data from table ',table.name);
            try {
                let data = await processEvent(event.Records);
                context.succeed(data);
            } catch (e) {
                console.log(e);
                context.done(e);
            }  
    };
};
let processEvent = async (Records) => {
    // Get Google credentials:
    let googleCreds = await getGoogleCredentials();
    // Run a BigQuery job:
    try {
        let results = await queryBQ(googleCreds);
        console.log(results);
        // Save to AWS S3:
        save(results);
    } catch (error) {
        console.log(error);
      };
    return 'Success'  ;
};
let getGoogleCredentials = async () => {
    let creds = await s3.getObject({Bucket: config.cred_bucket, Key: config.cred_s3_obj}).promise();
    var chunk = creds.Body.toString();
    let googleKey = JSON.parse(chunk).private_key.split("\\n");
    return googleKey;
}
;
// Queries BigQuery
let queryBQ = async (googleCreds) => {
    //Create GCS object
    const bigquery = new BigQuery({
        projectId: config.gcp_project_id, 
        credentials: { 
            client_email: config.gcp_client_email, 
            private_key: googleCreds[0]
        }
    });
    const query = `SELECT * FROM \`palrinyour-project-name.dataset-name.table\`;`
    const options = {
        query: query,
        // Location must match that of the dataset(s) referenced in the query.
        location: 'europe-west2',
      };
    try {
        // Run the query as a job
        const [job] = await bigquery.createQueryJob(options);
        console.log(`Job ${job.id} started.`);
        // Wait for the query to finish
        const [rows] = await job.getQueryResults();
        // Print the results and save to S3:
        // console.log('Rows:');
        rows.forEach(row => console.log(row));
        let res = rows.reduce( (acc,val) => { return acc + `${val.column_1},${val.column_2}\n`}, "");
        return `column_1,column_2\n`.concat(res);
    } catch (error) {
        console.log(error);
    }
};
// Function to save results to AWS S3:
let save = (results) => {
    let key = "your_folder_name/your_results_file.csv";
    return new Promise((resolve, reject) => {
        let params = {
            Bucket: "your.bucket_name.aws",
            Key: key,
            Body: results,
            CacheControl: "no-cache",
            ACL: 'public-read'
        };
        s3.putObject(params, (err, response) => {
            if (err) {
                console.log("Error writing to s3 with key " + key + ": ", err);
                reject(err);
            } else {
                console.log("Success writing to " + key);
                resolve(key);
            }
        });
    });
};