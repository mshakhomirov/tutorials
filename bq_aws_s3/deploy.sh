#!/usr/bin/env bash
# chmod +x deploy.sh
# Run ./deploy.sh -staging
# Run ./deploy.sh -production
ENVIRONMENT=$1
# ENVIRONMENT='-staging'
base=${PWD##*/}
zp=$base".zip"
echo $zp

rm -f $zp

zip -r $zp * -x deploy.sh
if ! aws lambda create-function  \
    --function-name bq-aws-s3${ENVIRONMENT} \
    --description "Sync files from BigQuery to AWS S3" \
    --environment Variables="{ENVIRONMENT="${ENVIRONMENT}"}" \
    --handler index.handler \
    --runtime nodejs12.x \
    --role arn:aws:iam::12345:role/your_lambda_role \
    --zip-file fileb://$zp;

    then

    echo ""
    echo "Function already exists, updating instead..."

    aws lambda update-function-code  \
    --function-name bq-aws-s3${ENVIRONMENT} \
    --zip-file fileb://$zp;
fi